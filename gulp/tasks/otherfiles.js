var gulp = require('gulp'),
    config = require('../config'),
    plumber = require('gulp-plumber'),
    chalk = require('chalk'),
    handleErrors = require('../util/handleErrors');



gulp.task('otherfiles', function () {
    console.log(chalk.blue('» copying other assets files...'));
    return gulp.src([
                      config.path.assetspath + '/**/*',
                      '!' + config.path.assetspath + '/images',
                      '!' + config.path.assetspath + '/scss',
                      '!' + config.path.assetspath + '/js'
        ])
        .pipe(plumber())
        .on('error', handleErrors)
        .pipe(gulp.dest(config.destPath + '/' + config.path.buildassetspath)) //output directory
});



